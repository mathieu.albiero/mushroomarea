#ifndef POSITION_H
#define POSITION_H

#include<iostream>
#include<QObject>
#include <QVariant>

class Position: public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint64 latitude   READ latitude   WRITE setLatitude   NOTIFY latitudeChanged)
    Q_PROPERTY(qint64 longitude   READ longitude   WRITE setLongitude   NOTIFY longitudeChanged)
private:
    qint64 m_latitude;
    qint64 m_longitude;
public:
    Position();
    Position(const long & latitude, const long & longitude, QObject *parent);
    Position(const Position& pos);
    friend std::ostream& operator<<(std::ostream& os, const Position& obj);
    qint64 latitude() const;
    qint64 longitude() const;
    Position& operator=(const Position &pos);

public slots:
    void setLatitude(qint64 latitude);
    void setLongitude(qint64 longitude);

signals:

    void latitudeChanged(long latitude);
    void longitudeChanged(long longitude);
};

#endif // POSITION_H
