#ifndef MUSHROOMLIST_H
#define MUSHROOMLIST_H

#include "mushroom.h"
#include <QVector>
#include <QObject>

class MushroomList : public QObject
{
    Q_OBJECT

private:
    QVector<Mushroom *> m_listOfMushrooms;
public:
    MushroomList(QObject *parent = nullptr);
    MushroomList(const MushroomList& mushroomList);
    MushroomList(const QVector<Mushroom*> mushroomList);
    void setListOfMushrooms(QVector<Mushroom*> mushroomList);
    QVector<Mushroom *> listOfMushrooms() const;
    Mushroom& operator [](int idx);
    Mushroom operator [](int idx) const;
public slots:
    bool addItem(Mushroom *mushroom);
    bool addItemWithIndex(int index, Mushroom *mushroom);
    bool deleteItemWithIndex(int index);
signals:
    void beginRemoveRow(int index);
    void endRemoveRow();
};

#endif // MUSHROOMLIST_H
