#include "customimage.h"

CustomImage::CustomImage(const QString &id, const QByteArray &data): m_id(id), m_data(data)
{
}

void CustomImage::setData(const QByteArray &data){
    this->m_data = data;
}

void CustomImage::setId(const QString &id){
    this->m_id = id;
}

QString CustomImage::id() const{
    return this->m_id;
}

QByteArray CustomImage::data() const{
    return this->m_data;
}
