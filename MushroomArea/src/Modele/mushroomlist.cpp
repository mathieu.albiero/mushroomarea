#include "mushroomlist.h"
#include <iostream>

using namespace std;

MushroomList::MushroomList(QObject *parent) : QObject(parent)
{
}

QVector<Mushroom *> MushroomList::listOfMushrooms() const{
    return m_listOfMushrooms;
}

bool MushroomList::addItem(Mushroom *mushroom){

    cout << "Ajout d'un champi : " << mushroom << endl;

    if(!m_listOfMushrooms.contains(mushroom)){
        m_listOfMushrooms.append(mushroom);
        return true;
    }
    return false;

}

bool MushroomList::deleteItemWithIndex(int index){
    if(m_listOfMushrooms.count() > index){
        m_listOfMushrooms.removeAt(index);
        return true;
    }
    return false;
}

MushroomList::MushroomList(const MushroomList& mushroomList): QObject(mushroomList.parent()){
    this->m_listOfMushrooms = mushroomList.listOfMushrooms();
}

MushroomList::MushroomList(const QVector<Mushroom*> mushroomList) : m_listOfMushrooms(mushroomList) {

}

bool MushroomList::addItemWithIndex(int index, Mushroom *mushroom){
    emit beginRemoveRow(index);

    this->m_listOfMushrooms.insert(index, mushroom);

    emit endRemoveRow();
    return true;
}

Mushroom& MushroomList::operator [](int idx) {
    return *(this->m_listOfMushrooms[idx]);
}

Mushroom MushroomList::operator [](int idx) const {
    return *(this->m_listOfMushrooms[idx]);
}

void MushroomList::setListOfMushrooms(QVector<Mushroom*> mushroomList){

    this->m_listOfMushrooms = mushroomList;
}



