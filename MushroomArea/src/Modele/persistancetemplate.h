#ifndef PERSISTANCETEMPLATE_H
#define PERSISTANCETEMPLATE_H

#include "arealist.h"


class PersistanceTemplate: public QObject
{
    Q_OBJECT
private:
    AreaList *m_areaList;
public:
    PersistanceTemplate();
    virtual void load() = 0;
    virtual void save() = 0;
    virtual AreaList * areas() = 0;
};

#endif // PERSISTANCETEMPLATE_H
