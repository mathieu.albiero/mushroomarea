#include "arealist.h"
#include <iostream>

using namespace std;

AreaList::AreaList(QObject *parent) : QObject(parent)
{
}

QVector<Area *> AreaList::listOfAreas(){
    return this->_listOfAreas;
}

bool AreaList::addItem(Area *area){
    if(!_listOfAreas.contains(area)){
        cout << "Ajout d'une zone : " << area << endl;
        _listOfAreas.append(area);
        return true;
    }
    return false;
}

bool AreaList::deleteItemWithIndex(int index){
    if(this->_listOfAreas.count() > index){
        this->_listOfAreas.removeAt(index);
        return true;
    }
    return false;
}

AreaList::AreaList(const AreaList& areaList): QObject(areaList.parent()){
    this->_listOfAreas = areaList._listOfAreas;
}

bool AreaList::addItemWithIndex(int index, Area *area){

    emit beginRemoveRow(index);

    this->_listOfAreas.insert(index, area);


    emit endRemoveRow();
    return true;
}

Area& AreaList::operator [](int idx) {
    return *(this->_listOfAreas[idx]);
}

Area AreaList::operator [](int idx) const {
    return *(this->_listOfAreas[idx]);
}

void AreaList::setListOfAreas(std::vector<Area *> areas){
    for(auto el : areas){
        this->_listOfAreas.push_back(el);
    }
}
