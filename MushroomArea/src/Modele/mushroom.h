#ifndef MUSHROOM_H
#define MUSHROOM_H

#include<iostream>
#include <QObject>



class Mushroom: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name   READ name   WRITE setName   NOTIFY nameChanged)
    Q_PROPERTY(QString urlImage   READ urlImage   WRITE setUrlImage   NOTIFY urlImageChanged)


private:
    QString m_name;
    QString m_urlImage;

public:
    Mushroom(QString name, QString image);
    Mushroom(const Mushroom& mush);
    friend std::ostream& operator<<(std::ostream& os, const Mushroom& obj);
    QString name() const;
    QString urlImage() const;
    Mushroom& operator=(const Mushroom& mush);

public slots:

    void setName(QString name);
    void setUrlImage(QString imageUrl);

signals:

    void nameChanged(QString name);
    void urlImageChanged(QString urlImage);
};

#endif // MUSHROOM_H
