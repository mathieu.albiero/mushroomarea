#ifndef JSONPERSISTANCE_H
#define JSONPERSISTANCE_H

#include "persistancetemplate.h"
#include "area.h"


class JsonPersistance: public PersistanceTemplate
{
private:
    AreaList * m_listArea;
    QString m_fileName;
    std::vector<Area *> parse(const QByteArray &data);
public:
    JsonPersistance(QString fileName);
    void load();
    void save();
    AreaList *areas();
};

#endif // JSONPERSISTANCE_H
