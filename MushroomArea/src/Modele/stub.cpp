#include "stub.h"
#include <QString>
#include <iostream>

using namespace std;

Stub::Stub()
{

}

AreaList *Stub::areas(){
    AreaList *result = new AreaList(nullptr);

    Area * area1 = new Area(QString("Toto"),QString("Toto"),QString("Toto"));
    area1->addItem(new Mushroom("girolle", "image"));
    area1->addItem(new Mushroom("cèpe", "image"));
    //cout << "Liste de champi : " << area1->mushrooms() << endl;
    cout << "Premier champi : " << area1->mushrooms()->listOfMushrooms()[0] << endl;
    result->addItem(area1);

    /*
    result->addItem(new Area(QString("Titi"),QString("Titi"),QString("Titi")));
    result->addItem(new Area(QString("Tutu"),QString("Tutu"),QString("Tutu")));
    result->addItem(new Area(QString("Tata"),QString("Tata"),QString("Tata")));
    result->addItem(new Area(QString("Tete"),QString("Tete"),QString("Tete")));
    */

    return result;
}

MushroomList *Stub::mushrooms(){
    MushroomList *result = new MushroomList(nullptr);
    cout << "Liste de champi : " << result << endl;
    result->addItem(new Mushroom("girolle", "image"));
    result->addItem(new Mushroom("cèpe", "image"));
    result->addItem(new Mushroom("truffe", "image"));

    return result;
}

void Stub::save(){

}

void Stub::load(){

}
