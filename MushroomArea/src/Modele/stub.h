#ifndef STUB_H
#define STUB_H

#include <QVector>
#include "area.h"
#include "arealist.h"
#include "persistancetemplate.h"

class Stub: public PersistanceTemplate
{
public:
    Stub();
    AreaList *areas();
    void load();
    void save();
    MushroomList *mushrooms();
};

#endif // STUB_H
