#include "jsonpersistance.h"

#include <QFile>
#include <QString>
#include <vector>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <iostream>

using namespace std;

JsonPersistance::JsonPersistance(QString fileName):
    PersistanceTemplate(), m_fileName(fileName)
{
    m_listArea = new AreaList();
    this->load();
}


void JsonPersistance::load(){
    QFile file(this->m_fileName);



    if (file.exists()) {
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            throw "Impossible d'ouvrir le fichier " + m_fileName;

        this->m_listArea->setListOfAreas(this->parse(file.readAll()));


    }
}

std::vector<Area *> JsonPersistance::parse(const QByteArray &data){
    QJsonDocument jsonDoc { QJsonDocument::fromJson(data)};
    QJsonObject jsonObj { jsonDoc.object() };

    std::vector<Area *> result;

    if (jsonObj.contains("listOfAreas") && jsonObj["listOfAreas"].isArray()) {
        QJsonArray listOfAreas = jsonObj["listOfAreas"].toArray();

        for (auto it : listOfAreas) {
            auto itemConverted = new Area(QString(it.toObject()["name"].toString()), QString(it.toObject()["description"].toString()), QString(it.toObject()["image"].toString()));

            /*
            if (it.toObject().contains("mushrooms") && it.toObject()["mushrooms"].isArray()) {
                QJsonArray mushrooms = it.toObject()["mushrooms"].toArray();
                QVector<Mushroom*> mushroomsResult;
                for (auto mush : mushrooms) {
                    mushroomsResult.push_back(new Mushroom(QString(mush.toObject()["name"].toString()), QString(mush.toObject()["imageUrl"].toString())));
                }
                itemConverted->setMushrooms(mushroomsResult);
            }
            */

            result.push_back(itemConverted);
        }
    }

    return result;
}

void JsonPersistance::save(){
    QFile file(this->m_fileName);

    if (!file.open(QIODevice::ReadWrite))
        throw "Impossible d'ouvrir le fichier " + this->m_fileName;

    QJsonObject jsonObj;
    QJsonArray jsonArray;

    for (Area* area : this->m_listArea->listOfAreas()) {
        QJsonObject position;
        QJsonObject areaConverted;
        QJsonArray listOfMushrooms;
        areaConverted["name"] = area->name();
        areaConverted["description"] = area->description();
        areaConverted["image"] = area->urlImage();



        position["latitude"] = area->position().latitude();
        position["longitude"] = area->position().longitude();

        areaConverted["position"] = position;

        /*
        for(auto mush: area->mushrooms()->listOfMushrooms()){
            QJsonObject mushConverted;

            mushConverted["name"] = mush->name();
            mushConverted["urlImage"] = mush->urlImage();

            listOfMushrooms.append(mushConverted);
        }
        */


        areaConverted["mushrooms"] = listOfMushrooms;

        jsonArray.append(areaConverted);
    }

    jsonObj["listOfAreas"] = jsonArray;

    QJsonDocument document (jsonObj);

    QByteArray rawData = document.toJson();

    file.write(rawData);

    file.close();
}

AreaList *JsonPersistance::areas(){
    return this->m_listArea;
}
