#include "area.h"

const Position &Area::position() const
{
    return m_position;
}

QString Area::name() const
{
    return m_name;
}

void Area::setName(QString newName)
{
    m_name = newName;
}

void Area::setPosition(Position position)
{
    m_position = position;
}

QString Area::description() const
{
    return m_description;
}

void Area::setMushrooms(MushroomList mushrooms){
    m_mushrooms = &mushrooms;
}

void Area::setMushrooms(QVector<Mushroom *> mushrooms){
    m_mushrooms->setListOfMushrooms(mushrooms);
}

void Area::setDescription(QString newDescription)
{
    m_description = newDescription;
}

QString Area::urlImage() const
{
    return m_urlImage;
}

void Area::setUrlImage(QString newUrlImage)
{
    m_urlImage = newUrlImage;
}

MushroomList *Area::mushrooms() const
{
    return m_mushrooms;
}

bool Area::addItem(Mushroom *mushroom){
    if(m_mushrooms->addItem(mushroom)){
        return true;
    }
    return false;
}

bool Area::deleteItemWithIndex(int index){
    if(this->m_mushrooms->listOfMushrooms().count() > index){
        this->m_mushrooms->listOfMushrooms().removeAt(index);
        return true;
    }
    return false;
}

bool Area::addItemWithIndex(int index, Mushroom *mushroom){
    emit beginRemoveRow(index);

    this->m_mushrooms->listOfMushrooms().insert(index, mushroom);

    emit endRemoveRow();
    return true;
}

std::ostream& operator<<(std::ostream& os, const Area& obj)
{
    os << obj.name().toStdString() << " : " << obj.description().toStdString() << "(x:" << obj.position().latitude() << ", y:" << obj.position().longitude() << ")";
    return os;
}

Area::Area() : m_position(0,0, this)
{
}

Area::Area(const Area& area): QObject(area.parent()){
    this->m_description = area.description();
    this->m_name = area.name();
    this->m_position = area.position();
    this->m_mushrooms = area.mushrooms();
    this->m_urlImage = area.urlImage();
}

Area::Area(QString name, QString description, QString image): m_position(0,0,this){
    this->m_name = name;
    this->m_description = description;
    if(image != NULL){
        this->m_urlImage = image;
    }
}
