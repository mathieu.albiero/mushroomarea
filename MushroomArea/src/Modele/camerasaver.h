#ifndef CAMERASAVER_H
#define CAMERASAVER_H

#include <QObject>
#include "customimage.h"


class CameraSaver: public QObject
{
    Q_OBJECT

public:
    CameraSaver(QObject *parent = nullptr);
    Q_INVOKABLE bool savePicture(const QString &id, QObject *objectImage);
    Q_INVOKABLE bool writePictures();
private :
    QList<CustomImage> m_images;
};

#endif // CAMERASAVER_H
