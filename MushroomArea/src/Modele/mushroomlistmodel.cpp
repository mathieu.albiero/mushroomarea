#include "mushroomlistmodel.h"
#include <iostream>

using namespace std;

MushroomListModel::MushroomListModel(QObject *parent)
{
    cout << "constructeur" << endl;
}

QHash<int, QByteArray> MushroomListModel::roleNames() const {
    cout << "role names mushroom" << endl;
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[ImageRole] = "image";
    return roles;
}

int MushroomListModel::rowCount(const QModelIndex &parent) const {
    return m_listOfMushroom->listOfMushrooms().count();
}

QVariant MushroomListModel::data(const QModelIndex &index, int role) const {
    cout << "Get data" << endl;
    if(index.row() < 0 || index.row() >= m_listOfMushroom->listOfMushrooms().count())
        return QVariant();
    Mushroom mushroom = m_listOfMushroom->operator[](index.row());
    if(role == NameRole){
        cout << "NAMEROLE" << endl;
        return mushroom.name();
    }
    else if(role == ImageRole)
        return mushroom.urlImage();
    return QVariant();
}

bool MushroomListModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if(index.row() < 0 || index.row() >= m_listOfMushroom->listOfMushrooms().count())
        return false;

    if(data(index,role) == value)
        return true;

    Mushroom mushroom = m_listOfMushroom->operator[](index.row());
    if(role == NameRole)
        mushroom.setName(value.toString());

    if(role == ImageRole)
        mushroom.setUrlImage(value.toString());

    QVector<int> roles;
    roles.append(role);

    QModelIndex topLeft = index.sibling(0, 0);
    QModelIndex bottomRight = index.sibling(m_listOfMushroom->listOfMushrooms().count()-1, 0);

    emit dataChanged(topLeft, bottomRight, roles);

    return true;
}

void MushroomListModel::addMushroom(Mushroom *mushroom) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());

    this->m_listOfMushroom->addItem(mushroom);

    endInsertRows();
}

bool MushroomListModel::insertRows(int row, int count, const QModelIndex &parent) {
    beginInsertRows(parent, row, row + count -1);

    for(int nb = 0; nb < count; ++nb) {
        m_listOfMushroom->addItemWithIndex(row, new Mushroom("default name", "unknow"));
    }

    endInsertRows();
    return true;
}

bool MushroomListModel::removeRows(int row, int count, const QModelIndex &parent) {
    if(row < 0 || row + count >= m_listOfMushroom->listOfMushrooms().count())
        return false;

    beginRemoveRows(parent, row, row+count-1);

    for(int nb = 0; nb < count; ++nb) {
        m_listOfMushroom->deleteItemWithIndex(row);
    }

    endRemoveRows();
    return true;
}

MushroomList* MushroomListModel::listOfMushrooms() const{
    return this->m_listOfMushroom;
}

void MushroomListModel::setListOfMushroom(MushroomList *list){

    this->m_listOfMushroom = list;
}

