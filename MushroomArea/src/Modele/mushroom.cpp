#include "mushroom.h"

QString Mushroom::name() const
{
    return m_name;
}

void Mushroom::setName(QString newName)
{
    m_name = newName;
}

QString Mushroom::urlImage() const
{
    return m_urlImage;
}

void Mushroom::setUrlImage(QString newUrlImage)
{
    m_urlImage = newUrlImage;
}

Mushroom::Mushroom(QString name, QString image)
{
    m_name = name;
    m_urlImage = image;
}

Mushroom& Mushroom::operator=(const Mushroom& mush){
    this->m_name = mush.name();
    this->m_urlImage = mush.urlImage();

    return *this;
}

Mushroom::Mushroom(const Mushroom& mush): QObject(mush.parent()){
    this->m_name = mush.name();
    this->m_urlImage = mush.urlImage();
}
