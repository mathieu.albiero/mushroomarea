#ifndef AREALISTMODEL_H
#define AREALISTMODEL_H

#include <QAbstractListModel>
#include "area.h"
#include "arealist.h"


class AreaListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(AreaList *listOfAreas READ listOfAreas WRITE setListOfAreas)
public:
    enum AreaRoles {
        NameRole,
        DescriptionRole,
        MushroomsRole,
        ImageRole
    };
    AreaListModel( QObject *parent = 0);
    Q_INVOKABLE void addArea(Area *area = nullptr);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::DisplayRole);

    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex & prent = QModelIndex());

    Q_INVOKABLE bool insertRows(int row, int count, const QModelIndex & parent = QModelIndex());

    AreaList* listOfAreas() const;
    void setListOfAreas(AreaList *list);

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    AreaList *m_listOfAreas;
};

#endif // AREALISTMODEL_H
