#include "arealistmodel.h"
#include <iostream>

using namespace std;

AreaListModel::AreaListModel(QObject *parent): QAbstractListModel(parent)
{
}

QHash<int, QByteArray> AreaListModel::roleNames() const {
    cout << "role names area" << endl;
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[DescriptionRole] = "description";
    roles[MushroomsRole] = "mushrooms";
    roles[ImageRole] = "image";
    return roles;
}

int AreaListModel::rowCount(const QModelIndex &) const {
    return m_listOfAreas->listOfAreas().count();
}

QVariant AreaListModel::data(const QModelIndex &index, int role) const {
    if(index.row() < 0 || index.row() >= m_listOfAreas->listOfAreas().count())
        return QVariant();
    Area area = m_listOfAreas->operator[](index.row());
    if(role == NameRole)
        return area.name();
    if(role == DescriptionRole)
        return area.description();
    if(role == ImageRole)
        return area.urlImage();
    if(role == MushroomsRole)
        //return QVariant::fromValue(area.mushrooms());
        return QVariant();
    return QVariant();
}

bool AreaListModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if(index.row() < 0 || index.row() >= m_listOfAreas->listOfAreas().count())
        return false;

    if(data(index,role) == value)
        return true;

    Area area = m_listOfAreas->operator[](index.row());
    if(role == NameRole){
        cout << "passage dans setData" << endl;
        area.setName(value.toString());
    }
    if(role == DescriptionRole)
        area.setDescription(value.toString());
    if(role == ImageRole)
        area.setUrlImage(value.toString());

    QVector<int> roles;
    roles.append(role);

    QModelIndex topLeft = index.sibling(0, 0);
    QModelIndex bottomRight = index.sibling(m_listOfAreas->listOfAreas().count()-1, 0);

    emit dataChanged(topLeft, bottomRight, roles);

    return true;
}

void AreaListModel::addArea(Area *area) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());

    if(area != nullptr){
        this->m_listOfAreas->addItem(area);
    }
    else{
        this->m_listOfAreas->addItem(new Area());
    }

    endInsertRows();
}

bool AreaListModel::insertRows(int row, int count, const QModelIndex &parent) {
    beginInsertRows(parent, row, row + count -1);

    for(int nb = 0; nb < count; ++nb) {
        m_listOfAreas->addItemWithIndex(row, new Area());
    }

    endInsertRows();
    return true;
}

bool AreaListModel::removeRows(int row, int count, const QModelIndex &parent) {
    if(row < 0 || row + count >= m_listOfAreas->listOfAreas().count())
        return false;

    beginRemoveRows(parent, row, row+count-1);

    for(int nb = 0; nb < count; ++nb) {
        m_listOfAreas->deleteItemWithIndex(row);
    }

    endRemoveRows();
    return true;
}

AreaList* AreaListModel::listOfAreas() const{
    return this->m_listOfAreas;
}

void AreaListModel::setListOfAreas(AreaList *list){
    this->m_listOfAreas = list;
}
