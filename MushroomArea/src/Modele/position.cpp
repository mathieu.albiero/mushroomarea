#include "position.h"

qint64 Position::latitude() const
{
    return m_latitude;
}

void Position::setLatitude(qint64 newLatitude)
{
    m_latitude = newLatitude;
}

qint64 Position::longitude() const
{
    return m_longitude;
}

void Position::setLongitude(qint64 newLongitude)
{
    m_longitude = newLongitude;
}

Position::Position(): m_latitude(0), m_longitude(0){

}

Position::Position(const Position& pos){
  this->m_latitude = pos.latitude();
  this->m_longitude = pos.longitude();
}

Position& Position::operator=(const Position &pos){
    this->m_latitude = pos.latitude();
    this->m_longitude = pos.longitude();
    return *this;
}

Position::Position(const long & latitude, const long & longitude, QObject *parent) : m_latitude(latitude), m_longitude(longitude), QObject(parent)
{
}
