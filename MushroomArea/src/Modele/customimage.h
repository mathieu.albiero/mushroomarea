#ifndef CUSTOMIMAGE_H
#define CUSTOMIMAGE_H

#include<QByteArray>
#include<QString>


class CustomImage
{
public:
    CustomImage(const QString &id = "", const QByteArray &data = nullptr);
    void setData(const QByteArray &data);
    void setId(const QString &id);
    QString id() const;
    QByteArray data() const;

private:
    QByteArray m_data;
    QString m_id;
};

#endif // CUSTOMIMAGE_H
