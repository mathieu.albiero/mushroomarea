#include "camerasaver.h"

#include<QQuickItemGrabResult>
#include<QBuffer>
#include<QFile>
#include<QStandardPaths>
#include<QDebug>

CameraSaver::CameraSaver(QObject *parent): QObject(parent)
{
}

bool CameraSaver::savePicture(const QString &id, QObject *objectImage)
{
    const int nbImages = this->m_images.size();

    for(int i = 0; i < nbImages; ++i){
        if(this->m_images.at(i).id() == id){
            return false;
        }
    }

    QQuickItemGrabResult *item = static_cast<QQuickItemGrabResult *>(objectImage);
    QByteArray imageData;
    QBuffer buffer(&imageData);

    if(item->image().save(&buffer, "PNG")){
        CustomImage image;
        image.setData(imageData);
        image.setId(id);
        this->m_images.append(image);
        return true;
    }

    return false;
}

bool CameraSaver::writePictures()
{
    const int nbImages = this->m_images.size();

    for(int i = 0; i < nbImages; ++i){
        QFile file;
        QString filename = this->m_images.at(i).id().split("/").last() + ".png";
        QString  directory = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        QString path = directory + "/" + filename;
        file.setFileName(path);
        if(file.open(QIODevice::WriteOnly)){
            if(file.write(this->m_images.at(i).data()) > 0){
                file.flush();
                file.close();
            }
            else{
                qDebug() << "Erreur : " << file.errorString();
                return false;
            }
        }
        else{
            qDebug() << "Erreur : " << file.errorString();
            return false;
        }
    }

    return true;
}
