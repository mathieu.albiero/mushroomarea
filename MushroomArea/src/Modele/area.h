#ifndef AREA_H
#define AREA_H

#include "position.h"
#include "mushroomlist.h"
#include "mushroom.h"
#include <QVector>
#include <QObject>

class Area: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name   READ name   WRITE setName   NOTIFY nameChanged)
    Q_PROPERTY(QString description   READ description   WRITE setDescription   NOTIFY descriptionChanged)
    Q_PROPERTY(QString urlImage   READ urlImage   WRITE setUrlImage   NOTIFY urlImageChanged)
    Q_PROPERTY(Position position   READ position   WRITE setPosition   NOTIFY positionChanged)

private:
    Position m_position;
    QString m_name = "Nouvelle zone";
    QString m_description = "Description ici";
    QString m_urlImage = "https://us.123rf.com/450wm/jemastock/jemastock1709/jemastock170912385/85758965-dise%C3%B1o-del-ejemplo-del-vector-de-la-imagen-del-icono-del-bosque-del-%C3%A1rbol-de-pino.jpg";
    MushroomList * m_mushrooms;

public:
    Area();
    Area(const Area& area);
    Area(QString name, QString description, QString image = NULL);
    const Position &position() const;
    QString name() const;
    QString description() const;
    QString urlImage() const;
    void setMushrooms(MushroomList mushrooms);
    void setMushrooms(QVector<Mushroom*> mushrooms);
    MushroomList * mushrooms() const;
    friend std::ostream& operator<<(std::ostream& os, const Area& obj);

public slots:

    void setPosition(Position position);
    void setName(QString name);
    void setDescription(QString description);
    void setUrlImage(QString imageUrl);
    bool addItem(Mushroom *mushroom);
    bool addItemWithIndex(int index, Mushroom *mushroom);
    bool deleteItemWithIndex(int index);

signals:

    void positionChanged(Position position);
    void nameChanged(QString name);
    void descriptionChanged(int volume);
    void urlImageChanged(QString urlImage);
    void mushroomListChanged(MushroomList mushroom);
    void beginRemoveRow(int index);
    void endRemoveRow();

private:
};




#endif // AREA_H
