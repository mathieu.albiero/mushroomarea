#ifndef AREALIST_H
#define AREALIST_H

#include <QVector>
#include<QObject>
#include "area.h"

class AreaList: public QObject
{
    Q_OBJECT
private:
    QVector<Area *> _listOfAreas;
public:
    AreaList(QObject *parent = nullptr);
    AreaList(const AreaList& areaList);
    QVector<Area *> listOfAreas();
    void setListOfAreas(std::vector<Area *> areas);
    Area& operator [](int idx);
    Area operator [](int idx) const;
public slots:
    bool addItem(Area *area);
    bool addItemWithIndex(int index, Area *area);
    bool deleteItemWithIndex(int index);
signals:
    void beginRemoveRow(int index);
    void endRemoveRow();
};

#endif // AREALIST_H
