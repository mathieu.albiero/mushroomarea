#ifndef MUSHROOMLISTMODEL_H
#define MUSHROOMLISTMODEL_H

#include <QAbstractListModel>
#include "mushroom.h"
#include "mushroomlist.h"

class MushroomListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(MushroomList *listOfMushrooms READ listOfMushrooms WRITE setListOfMushroom)
public:
    enum MushroomRoles {
        NameRole,
        ImageRole
    };
    MushroomListModel( QObject *parent = 0);
    void addMushroom(Mushroom *mushroom);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::DisplayRole);

    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex & prent = QModelIndex());

    Q_INVOKABLE bool insertRows(int row, int count, const QModelIndex & parent = QModelIndex());

    MushroomList* listOfMushrooms() const;
    void setListOfMushroom(MushroomList *list);

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    MushroomList *m_listOfMushroom;
};

#endif // MUSHROOMLISTMODEL_H
