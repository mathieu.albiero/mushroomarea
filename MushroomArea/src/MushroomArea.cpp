#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include "Modele/arealist.h"
#include "Modele/arealistmodel.h"
#include "Modele/mushroomlistmodel.h"
#include "Modele/mushroomlist.h"
#include "Modele/area.h"
#include "Modele/position.h"
#include "Modele/stub.h"
#include "Modele/jsonpersistance.h"
#include "Modele/camerasaver.h"

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/MushroomArea.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    Stub stub;

    QGuiApplication * app = SailfishApp::application(argc, argv);
    QQuickView * view     = SailfishApp::createView();

    Stub areasStub;
    JsonPersistance areasJSON(QString("/tmp/save.json"));

    CameraSaver cameraSaver;

    qmlRegisterType<AreaListModel>("AreaListModel", 1, 0, "AreaListModel");
    qmlRegisterType<MushroomListModel>("MushroomListModel", 1, 0, "MushroomListModel");
    qmlRegisterType<MushroomList>("MushroomList", 1, 0, "MushroomList");
    qmlRegisterType<Area>("Area",1 ,0 , "Area");

    view->rootContext()->setContextProperty(QStringLiteral("stubListAreas"), areasJSON.areas());
    view->rootContext()->setContextProperty(QStringLiteral("CameraSaver"), &cameraSaver);

    view->rootContext()->setContextProperty(QStringLiteral("stubListMushrooms"), stub.mushrooms());

    view->setSource(SailfishApp::pathToMainQml());
    view->show();


    QObject::connect(app, &QGuiApplication::aboutToQuit, &areasJSON, &JsonPersistance::save);

    return app->exec();
}
