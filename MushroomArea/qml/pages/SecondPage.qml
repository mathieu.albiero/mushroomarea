import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.Layouts 1.0
import MushroomListModel 1.0
import MushroomList 1.0


Page {
    id: page

    property string areaName
    property string areaDescription
    property string areaImage
    property MushroomList mushroomsList

    Component.onCompleted:   console.log(mushroomsList);

    allowedOrientations: Orientation.All


    ColumnLayout{

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Image {
            id: image
            source: areaImage
            Layout.alignment: Qt.AlignCenter
        }

        Label {
            id: name
            text: qsTr(areaName)
            Layout.alignment: Qt.AlignCenter
        }

        Button{
            text: qsTr("Ouvrir dans google maps")
        }

        Text {
            text: qsTr("Description :")
            color: "white"
        }

        Label {
            id: description
            text: qsTr(areaDescription)
        }

        Text {            
            text: qsTr("Champignons pouvant être trouvés :")
            color: "white"
            Layout.alignment: Qt.AlignCenter
        }
        SilicaListView{
            id: detailListView

            populate: Transition{
                NumberAnimation { properties: "x,y"; duration: 300 }
            }

            width: parent.width
            height: parent.height

            model: MushroomListModel{
                listOfMushrooms : stubListMushrooms
                //Component.onCompleted: console.log(mushroomsList);
            }

            delegate: BackgroundItem {
                width: parent.width
                id: listItem

                Text {
                     id: content
                     width: parent.width
                     text: qsTr(name)
                     color: "white"
                     //Component.onCompleted: console.log(name);
                }
            }


        }
    }
}
