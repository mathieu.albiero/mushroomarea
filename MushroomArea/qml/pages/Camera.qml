import QtQuick 2.0
import QtMultimedia 5.6
import QtQuick.Layouts 1.1
import Sailfish.Silica 1.0


Page {
    id: root
    visible: true
    allowedOrientations: Orientation.All

    VideoOutput {
        source: camera

        Camera{
            id: camera
            //deviceId: QtMultimedia.availableCameras[0].deviceId
            // imageProcessing.whiteBalanceMode: CameraImageProcessing.whiteBalanceFlash
            exposure.exposureCompensation: -1.0
            exposure.exposureMode: Camera.ExposurePortrait
            flash.mode: Camera.FlashRedEyeReduction
            imageCapture.onImageCaptured: {
                var image = {
                    "id": preview,
                    "source": preview
                }

                picturesModel.push(image);
                root.picturesModelChanged();
            }
        }
    }
}

