import QtQuick 2.0
import Sailfish.Silica 1.0
import AreaListModel 1.0
import Area 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaListView{
        id: masterListView

        populate: Transition{
            NumberAnimation { properties: "x,y"; duration: 300 }
        }

        width: parent.width
        height: parent.height

        model: AreaListModel{
            listOfAreas: stubListAreas
        }

        delegate: ListItem {
            width: parent.width
            id: listItem

            RemorseItem { id: deletor }

            Image {
                id: listIcon
                width: parent.width/5
                height: width
                        source: qsTr(model.image)
                    }

                    Label {
                        padding: 40
                        anchors.left: listIcon.right
                        anchors.verticalCenter: listIcon.verticalCenter
                        horizontalAlignment: Text.AlignHCenter
                         text: qsTr(model.name) + " - " + qsTr(model.description)
                         color: "white"
                    }


            onPressAndHold: {
                deletor.execute(listItem, "Supprimer l'éléments ?", function() { masterListView.model.removeRows(index, 1); } )
            }

            onClicked: {
                   pageStack.push(Qt.resolvedUrl("SecondPage.qml"), {areaName: name, areaDescription: description, areaImage: image})
            }

        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Ajouter une zone")
                onClicked: {
                    masterListView.model.addArea();
                }


            }
            MenuItem {
                text: qsTr("Camera")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("Camera.qml"))
                }


            }
        }

    }
}
