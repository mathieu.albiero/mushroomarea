# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = MushroomArea

CONFIG += sailfishapp

SOURCES += src/MushroomArea.cpp \
    src/Modele/area.cpp \
    src/Modele/arealist.cpp \
    src/Modele/arealistmodel.cpp \
    src/Modele/camerasaver.cpp \
    src/Modele/customimage.cpp \
    src/Modele/jsonpersistance.cpp \
    src/Modele/mushroom.cpp \
    src/Modele/persistancetemplate.cpp \
    src/Modele/mushroomlist.cpp \
    src/Modele/mushroomlistmodel.cpp \
    src/Modele/position.cpp \
    src/Modele/stub.cpp

DISTFILES += qml/MushroomArea.qml \
    qml/cover/CoverPage.qml \
    qml/pages/Camera.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    rpm/MushroomArea.changes.in \
    rpm/MushroomArea.changes.run.in \
    rpm/MushroomArea.spec \
    rpm/MushroomArea.yaml \
    translations/*.ts \
    MushroomArea.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/MushroomArea-de.ts

HEADERS += \
    src/Modele/area.h \
    src/Modele/arealist.h \
    src/Modele/arealistmodel.h \
    src/Modele/camerasaver.h \
    src/Modele/customimage.h \
    src/Modele/jsonpersistance.h \
    src/Modele/mushroom.h \
    src/Modele/persistancetemplate.h \
    src/Modele/mushroomlist.h \
    src/Modele/mushroomlistmodel.h \
    src/Modele/position.h \
    src/Modele/stub.h


QT += multimedia
QTPLUGIN += qavfcamera
