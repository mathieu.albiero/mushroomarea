# MushroomArea

Fonctionne sur *SailfishOS*.

## Auteurs

### Théo RAMOUSSE : 50% du boulot.

- Métier C++ 50%
- Json persistance
- Master
- Caméra

### Mathieu ALBIERO: 50%

- Detail
- Métier C++ 50%

## Descriptif

Ce projet consiste en la réalisation d’une application smartphone permettant aux utilisateurs de sauvegarder leurs points pour cueillir des champignons.
Pour se faire, l’utilisateur disposera d’une interface graphique au format master/detail. Lorsque l’utilisateur se trouvera dans un lieu où il y a des champignons, il pourra renseigner ce lieu en remplissant les champs suivants : 
-	Un ou plusieurs types de champignons qui s’y trouve
-	Le nom qu’il souhaite donner au lieu
-	Une image du lieu (optionnel)
Si le type de champignon n’est pas disponible dans l’application, l’utilisateur pourra enrichir sa base de connaissances en renseignant un nouveau type de champignon. Voici les informations à renseigner pour le champignon : 
-	Nom
-	Image (optionnelle)
-	Description
Les coordonnées GPS du lieu seront renseignées par l'utilisateur au format (latitude/longitude).
Les données sont suavegardées dans un fichier JSON.


## Usage

Il suffit de lancer l'application sur Sailfish OS.

## Techniques de programmation utilisées

### QtCamera

Utilisation de QtCamera pour prendre des photos et affichage de la caméra

### QObject avec QProperties

Chaque area est un QObject avec plusieurs propriétés. La modification d'une des propriétés entraîne son rafraîchissement dans les listes.
Chaque mushroom est un QObject avec plusieurs propriétés. La modification d'une des propriétés entraîne son rafraîchissement dans les listes qui se trouve dans le detail d'une area.

### PullDownMenu

Un PullDownMenu est présent dans le master pour pouvoir ajouter une nouvelle area

### Clic

Un clic sur une area ouvre la page de detail.

### Master

Un clic long sur une area affiche une remorse bar pour supprimer l'area sélectionnée.

### Remorse

L'item précédent utilise une RemorseBar.

### Sauvegarde des différents items

Lors de l'ouverture de l'application, les objets qui se trouvent dans un fichier JSON sont chargés.
Lors de la fermeture de l'application, les objets sont sauvegardés dans le fichier JSON.
Utilisation de QFile, QJsonDocument, QJsonArray et QJsonObject

## Bugs connus

- La caméra ne fonctionne pas car nous n'avons pas pu tester sur un téléphone
- Affichage des champignons non fonctionnel (c'est un stub qui affiche les champignons et ceux présents dans le JSON)

## Précisions

Nous n'avons pas réussi à implémenter l'utilisation de la caméra car nous n'avons jamais réussi à démarrer l'application sur un vrai smartphone.

